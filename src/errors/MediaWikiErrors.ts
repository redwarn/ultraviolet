import { Page } from "app/mediawiki/core/Page";
import { Revision, User } from "app/mediawiki";
import UVErrorBase, { UVErrors, UVFormattedError } from "./UVError";

export class PageMissingError extends UVFormattedError<{ page: Page }> {
    readonly code = UVErrors.PageMissing;
    static readonly message = "The page {{page.title}} could not be found.";
}

export class PageInvalidError extends UVFormattedError<{
    page: Page;
    reason: string;
}> {
    readonly code = UVErrors.PageInvalid;
    static readonly message =
        "The page {{page.title}} could not be found. Reason: {{reason}}";
}

export class RevisionMissingError extends UVFormattedError<{ id: number }> {
    readonly code = UVErrors.RevisionMissing;
    static readonly message = "There is no revision with ID {{id}}.";
}

export class SectionIndexMissingError extends UVFormattedError<{
    sectionId: number;
    revision: Revision;
}> {
    readonly code = UVErrors.PageInvalid;
    static readonly message =
        "Revision with ID {{revision.revisionID}} does not contain a section with index {{sectionId}}.";
}

export class RevisionNotLatestError extends UVFormattedError<{
    revision: Revision;
    latest: Revision;
}> {
    readonly code = UVErrors.RevisionNotLatest;
    static readonly message =
        "Target revision {{revision.revisionID}} is not the latest revision.";
}

export class UserMissingError extends UVFormattedError<{ user: User }> {
    readonly code = UVErrors.UserMissing;
    static readonly message = "The user {{user.username}} could not be found.";
}

export class UserInvalidError extends UVFormattedError<{ user: User }> {
    readonly code = UVErrors.UserInvalid;
    static readonly message = "The username {{user.username}} is invalid.";
}

export class GenericAPIError extends UVErrorBase {
    readonly code = UVErrors.APIError;

    constructor(readonly error: Record<string, any> | string) {
        super();
    }

    get message() {
        return typeof this.error === "string"
            ? this.error
            : this.error["text"] ??
                  this.error["info"] ??
                  this.error["html"] ??
                  "Unknown MediaWiki API error.";
    }
}

export type GenericAPIErrorData = { page: Page } | { revision: Revision };

export const SpecializedMediaWikiErrors: Record<string, any> = {
    missingtitle: PageMissingError,
    nosuchrevid: RevisionMissingError,
};
