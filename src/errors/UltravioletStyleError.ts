import Style from "app/styles/Style";
import { UVErrors, UVFormattedError } from "./UVError";

export class StyleMissingError extends UVFormattedError<{ styleId: string }> {
    static readonly code = UVErrors.StyleMissing;
    static readonly message = 'The style "{{styleId}}" could not be found.';
}

export class StyleLoadError extends UVFormattedError<{ style: Style }> {
    static readonly code = UVErrors.StyleMissing;
    static readonly message =
        'An error ocurred while the style "{{style.name}}" was loading.';
}
