/**
 * Attempt to upgrade an outdated configuration file.
 */
import { APP_WIKI_CONFIGURATION_VERSION } from "app/data/UltravioletConstants";
import WikiConfigurationRaw from "app/config/wiki/WikiConfigurationRaw";

const upgraders: Record<
    number,
    (oldConfiguration: Record<string, any>) => Record<string, any>
> = {};

export default function (config: Record<string, any>): WikiConfigurationRaw {
    let newConfig: Record<string, any> = config;

    while (
        newConfig.configVersion < APP_WIKI_CONFIGURATION_VERSION &&
        upgraders[newConfig.configVersion] != null
    ) {
        newConfig = upgraders[newConfig.configVersion](newConfig);
    }

    if (newConfig.configVersion === APP_WIKI_CONFIGURATION_VERSION) {
        return newConfig as WikiConfigurationRaw;
    } else {
        // We ran out of valid upgraders.
        // TODO: Proper errors
        throw new Error(
            "Cannot upgrade wiki-configuration file: no valid configuration available."
        );
    }
}
