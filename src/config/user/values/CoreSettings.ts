/*
 * Ultraviolet core settings.
 */

import { APP_CONFIG_VERSION, APP_VERSION } from "app/data/UltravioletConstants";
import { ConfigurationSet } from "../Configuration";
import { Setting } from "../Setting";

export default function initCoreSettings(): ConfigurationSet {
    return {
        /** Last version of Ultraviolet/RedWarn that was used */
        latestVersion: new Setting("latestVersion", APP_VERSION, null),

        /** The configuration version, responsible for keeping track of configuration schema changes. */
        configVersion: new Setting("configVersion", APP_CONFIG_VERSION, null),

        /** Neopolitan. */
        neopolitan: new Setting("neopolitan", null),
    };
}
