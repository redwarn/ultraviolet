import UltravioletStore from "app/data/UltravioletStore";
import { UVUIDialog, UVUIDialogID } from "app/ui/elements/UVUIDialog";
import { StyleStorage } from "app/styles/Style";
import MaterialToast from "app/styles/material/ui/MaterialToast";

export class MaterialDialogTrackMap extends Map<UVUIDialogID, UVUIDialog<any>> {
    domRemove(key: UVUIDialogID): void {
        const remove = (i = 0) => {
            const dialogElement = this.get(key).element;

            if (
                !dialogElement.classList.contains("mdc-dialog--open") ||
                i >= 100
            )
                dialogElement.parentElement.removeChild(dialogElement);
            else
                setTimeout(() => {
                    remove(i++);
                }, 10);
        };
        setTimeout(remove, 2000);
    }

    delete(key: UVUIDialogID): boolean {
        // Delete dialog element after 2 seconds.
        setTimeout(() => {
            document
                .getElementById(key)
                ?.parentElement?.removeChild(document.getElementById(key));
        }, 2000);
        return super.delete(key);
    }
}

export class MaterialStyleStorage extends StyleStorage {
    // Caches
    dialogTracker: Map<UVUIDialogID, UVUIDialog<any>> =
        new MaterialDialogTrackMap();
    toastQueue: MaterialToast[] = [];

    mutationObserver: MutationObserver;
    intervals: any[] = [];
}

export function getMaterialStorage(): MaterialStyleStorage {
    return (
        (UltravioletStore.styleStorage as MaterialStyleStorage) ??
        (UltravioletStore.styleStorage = new MaterialStyleStorage())
    );
}
