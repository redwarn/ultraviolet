import StyleManager from "app/styles/StyleManager";
import { UVUIToast } from "./elements/UVUIToast";
import { UVUIDiffIcons } from "app/ui/elements/UVUIDiffIcons";
import { UVUIPageIcons } from "app/ui/elements/UVUIPageIcons";
import { UVUIAlertDialog } from "app/ui/elements/UVUIAlertDialog";
import { UVUIInputDialog } from "app/ui/elements/UVUIInputDialog";
import { UVUISelectionDialog } from "app/ui/elements/UVUISelectionDialog";
import { UVUIIFrameDialog } from "app/ui/elements/UVUIIFrameDialog";
import { UVUIWarnDialog } from "app/ui/elements/UVUIWarnDialog";
import { UVUIExtendedOptions } from "app/ui/elements/UVUIExtendedOptions";
import { UVUIProtectionRequestDialog } from "app/ui/elements/UVUIProtectionRequestDialog";
import { UVUIReportingDialog } from "app/ui/elements/UVUIReportingDialog";
import { UVUIPreferencesTab } from "app/ui/elements/UVUIPreferencesTab";
import { UVUIPreferencesItem } from "./elements/UVUIPreferencesItem";
import { UVUIPreferences } from "./elements/UVUIPreferences";

/**
 * Redirect class for easy access. UI elements of Ultraviolet are also created here.
 */
export default class UltravioletUI {
    /** Alias of {@link StyleManager.activeStyle.classMap.uvAlertDialog} */
    static get AlertDialog(): typeof UVUIAlertDialog {
        return StyleManager.activeStyle.classMap.uvAlertDialog;
    }
    /** Alias of {@link StyleManager.activeStyle.classMap.uvInputDialog} */
    static get InputDialog(): typeof UVUIInputDialog {
        return StyleManager.activeStyle.classMap.uvInputDialog;
    }
    /** Alias of {@link StyleManager.activeStyle.classMap.uvSelectionDialog} */
    static get SelectionDialog(): typeof UVUISelectionDialog {
        return StyleManager.activeStyle.classMap.uvSelectionDialog;
    }
    /** Alias of {@link StyleManager.activeStyle.classMap.uvWarnDialog} */
    static get WarnDialog(): typeof UVUIWarnDialog {
        return StyleManager.activeStyle.classMap.uvWarnDialog;
    }
    /** Alias of {@link StyleManager.activeStyle.classMap.uvProtectionRequestDialog} */
    static get ProtectionRequestDialog(): typeof UVUIProtectionRequestDialog {
        return StyleManager.activeStyle.classMap.uvProtectionRequestDialog;
    }
    /** Alias of {@link StyleManager.activeStyle.classMap.uvIFrameDialog} */
    static get IFrameDialog(): typeof UVUIIFrameDialog {
        return StyleManager.activeStyle.classMap.uvIFrameDialog;
    }
    /** Alias of {@link StyleManager.activeStyle.classMap.uvToast} */
    static get Toast(): typeof UVUIToast {
        return StyleManager.activeStyle.classMap.uvToast;
    }
    /** Alias of {@link StyleManager.activeStyle.classMap.uvDiffIcons} */
    static get DiffIcons(): typeof UVUIDiffIcons {
        return StyleManager.activeStyle.classMap.uvDiffIcons;
    }
    /** Alias of {@link StyleManager.activeStyle.classMap.uvPageIcons} */
    static get PageIcons(): typeof UVUIPageIcons {
        return StyleManager.activeStyle.classMap.uvPageIcons;
    }
    /** Alias of {@link StyleManager.activeStyle.classMap.uvExtendedOptions} */
    static get ExtendedOptions(): typeof UVUIExtendedOptions {
        return StyleManager.activeStyle.classMap.uvExtendedOptions;
    }
    /** Alias of {@link StyleManager.activeStyle.classMap.uvReportingDialog} */
    static get ReportingDialog(): typeof UVUIReportingDialog {
        return StyleManager.activeStyle.classMap.uvReportingDialog;
    }
    /** Alias of {@link StyleManager.activeStyle.classMap.uvPreferences} */
    static get Preferences(): typeof UVUIPreferences {
        return StyleManager.activeStyle.classMap.uvPreferences;
    }
    /** Alias of {@link StyleManager.activeStyle.classMap.uvPreferencesTab} */
    static get PreferencesTab(): typeof UVUIPreferencesTab {
        return StyleManager.activeStyle.classMap.uvPreferencesTab;
    }
    /** Alias of {@link StyleManager.activeStyle.classMap.uvPreferencesItem} */
    static get PreferencesItem(): typeof UVUIPreferencesItem {
        return StyleManager.activeStyle.classMap.uvPreferencesItem;
    }
}

/**
 * A complete list of all UVUIElements
 */
export const UVUIElements = {
    [UVUIAlertDialog.elementName]: UVUIAlertDialog,
    [UVUIInputDialog.elementName]: UVUIInputDialog,
    [UVUISelectionDialog.elementName]: UVUISelectionDialog,
    [UVUIWarnDialog.elementName]: UVUIWarnDialog,
    [UVUIProtectionRequestDialog.elementName]: UVUIProtectionRequestDialog,
    [UVUIIFrameDialog.elementName]: UVUIIFrameDialog,
    [UVUIToast.elementName]: UVUIToast,
    [UVUIDiffIcons.elementName]: UVUIDiffIcons,
    [UVUIPageIcons.elementName]: UVUIPageIcons,
    [UVUIExtendedOptions.elementName]: UVUIExtendedOptions,
    [UVUIReportingDialog.elementName]: UVUIReportingDialog,
    [UVUIPreferences.elementName]: UVUIPreferences,
    [UVUIPreferencesTab.elementName]: UVUIPreferencesTab,
    [UVUIPreferencesItem.elementName]: UVUIPreferencesItem,
};
