import { Injector } from "./Injector";
import { h } from "tsx-dom";
import UltravioletUI from "../UltravioletUI";
import UltravioletStore from "app/data/UltravioletStore";
import UltravioletHooks from "app/event/UltravioletHooks";

export default class PreferencesInjector implements Injector {
    async init(): Promise<void> {
        // Check if on Project:(RedWarn|Ultraviolet)/Preferences
        if (
            UltravioletStore.currentPage.title.namespace === 4 &&
            /^(RedWarn|Ultraviolet)\/Preferences$/.test(
                UltravioletStore.currentPage.title.title
            ) &&
            mw.config.get("wgAction") === "view"
        ) {
            const preferences = new UltravioletUI.Preferences({
                excludeTabs: ["core"],
            });
            const preferencesElement = preferences.render();

            const target = document.querySelector("#mw-content-text");
            if (!target) {
                // Not a valid MediaWiki page.
                throw "Could not find mw-content-text";
            }
            target.innerHTML = "";

            const targetId = target.getAttribute("id");
            const targetClass = target.getAttribute("class");
            target.removeAttribute("id");
            target.removeAttribute("class");
            target.appendChild(preferencesElement);

            UltravioletHooks.addHook("deinit", () => {
                preferencesElement.parentElement?.removeChild(
                    preferencesElement
                );
                target.setAttribute("id", targetId);
                target.setAttribute("class", targetClass);
            });
        }
    }
}
