import { Page } from "app/mediawiki";
import { UVUIDialog, UVUIDialogProperties } from "app/ui/elements/UVUIDialog";
import ProtectionRequest from "app/mediawiki/protection/ProtectionRequest";

export interface UVUIProtectionRequestDialogProps extends UVUIDialogProperties {
    relatedPage?: Page;
}

export class UVUIProtectionRequestDialog extends UVUIDialog<ProtectionRequest> {
    show(): Promise<ProtectionRequest> {
        throw new Error("Attempted to call abstract method");
    }
    render(): HTMLDialogElement {
        throw new Error("Attempted to call abstract method");
    }

    public static readonly elementName = "uvProtectionRequestDialog";

    constructor(readonly props: UVUIProtectionRequestDialogProps = {}) {
        super(props);
    }
}
