import UVUIElement, { UVUIElementProperties } from "./UVUIElement";

export interface UVUIPreferencesProperties extends UVUIElementProperties {
    excludeTabs?: string[];
}

export class UVUIPreferences extends UVUIElement {
    public static readonly elementName = "uvPreferences";

    /**
     * The HTMLDivElement which contains the tab.
     */
    element?: HTMLDivElement;

    constructor(readonly props: UVUIPreferencesProperties) {
        super();
    }

    /**
     * Renders the preferences tab.
     */
    render(): HTMLDivElement {
        throw new Error("Attempted to call abstract method");
    }
}
