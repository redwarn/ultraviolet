import { PrimitiveSetting, Setting } from "app/config/user/Setting";
import UVUIElement, { UVUIElementProperties } from "./UVUIElement";

export interface UVUIPreferencesTabProperties extends UVUIElementProperties {
    title: string;
    items: Setting<any>[];
    active: boolean;
    onChange: (setting: PrimitiveSetting<any>) => void;
}

export class UVUIPreferencesTab extends UVUIElement {
    public static readonly elementName = "uvPreferencesTab";

    /**
     * The HTMLDivElement which contains the tab.
     */
    element?: HTMLDivElement;

    /**
     * The HTMLButtonElement which contains the tab bar item.
     */
    tabBarElement?: HTMLButtonElement;

    constructor(readonly props: UVUIPreferencesTabProperties) {
        super();
    }

    /**
     * Renders the preferences tab.
     */
    render(): HTMLDivElement {
        throw new Error("Attempted to call abstract method");
    }

    /**
     * Renders the preferences tab bar item.
     */
    renderTabBarItem(): HTMLButtonElement {
        throw new Error("Attempted to call abstract method");
    }

    /**
     * Activates the tab.
     */
    activate(): void {
        throw new Error("Attempted to call abstract method");
    }
}
